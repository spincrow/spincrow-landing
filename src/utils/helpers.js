import addToMailChimp from "gatsby-plugin-mailchimp"
import swal from "sweetalert"


export const sendSubscriberMail = async (email) => {
    console.log(email)
    addToMailChimp(email) // listFields are optional if you are only capturing the email address.
    .then(data => {
      // I recommend setting data to React state
      // but you can do whatever you want (including ignoring this `then()` altogether)
      console.log(data)
      try {
          if(data.result == "success"){
              swal("Thanks !", "we would holla once we launch", "success")
          }else{
              throw new Error('extended updates');
          }
      }catch(err){
        swal("Yo", "we already have your email", "error");
      }
    })
    .catch(() => {
      // unnecessary because Mailchimp only ever
      // returns a 200 status code
      // see below for how to handle errors
    })
}
