import React, {Fragment} from "react"
import Header from "./Header"
import Footer from "./Footer"
import { useStaticQuery, Link, graphql } from "gatsby"

export default ({ children }) => {
    const data = useStaticQuery(
        graphql`
          query {
            site {
              siteMetadata {
                title
              }
            }
          }
        `
      )

    return (
        <Fragment>
            <div style={{
                backgroundImage: `url(${require('../assets/base-header.svg')})`,
                backgroundSize: 'cover',
                height: '130vh',
                width: '100%'
                }}>
            </div>
                <div className="container-fluid" style={{
                    position: 'absolute',
                    top: 0
                }}>
                <Header data={data} />
                    {children}
                    <div className="row">
                      <div className="col px-0">
                        <Footer/>
                      </div>
                    </div>
            </div>
            
        </Fragment>
    )
}