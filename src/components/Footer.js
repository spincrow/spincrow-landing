import React from "react"
import { Instagram, Facebook, Twitter} from "react-feather"
import {Link} from 'gatsby'

export default () => 
<footer className="bg-primary">
    <div className="container mt-5" >
        <div className="row">
            <div className="col-md-auto col-12 d-flex flex-row">
                <div className="justify-content-center mx-md-4 mx-auto mt-3 text-white">
                    <img src={require('../assets/Footer.svg')} /> Spincrow
                </div>
            </div>
            <div className="col d-flex flex-row">
            </div>
            <div className="col-md-auto col-12 my-3">
                <div className="d-flex justify-content-center">
                    <a className="text-white mx-2"> How it works</a>
                    <a className="text-white mx-2"> Support </a>
                    <a className="text-white mx-2"> Explore </a>
                </div>  
            </div>
        </div>
    </div>
</footer>