import React, { Fragment, useState } from "react";
import { Link } from "gatsby"
import {Menu} from "react-feather"
import SEO from "./SEO"
import favicon16 from "../assets/favicon/favicon-16x16.png";
import favicon32 from  "../assets/favicon/favicon-32x32.png";
import favicon64 from  "../assets/favicon/favicon-96x96.png";
import {sendSubscriberMail} from "../utils/helpers"
const img = require('../assets/Logo.png')

export default  ({data}) => {

    const [state, updateState] = useState({
        email: null
    })

    const sendMail = async () => {
        // console.log("called mail")
        let resp = await sendSubscriberMail(state.email)
        // console.log(resp)
        updateState({
            ...state,
            email: null
        })
    } 

    const updateMail = (e) => {
        updateState({
            ...state,
            email: e.target.value
        })
    }

    return (
        <Fragment>
            <SEO />
            <div className="container">
                <nav className="navbar navbar-expand-lg navbar-transparent bg-transparent nav">
                    <a className="navbar-brand" href="#">
                        <img src={img} className="navbar-brand-img" alt={data.site.siteMetadata.title}/> Spincrow <span className="badge badge-soft-success">Beta</span>
                    </a>
                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <Menu className="text-primary" />
                    </button>
                
                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul className="navbar-nav ml-auto mt-3">
                            <li className="nav-item mx-2">
                                <div className="input-group mb-3">
                                    <input type="text" value={state.email} onChange={updateMail} className="form-control" placeholder="Enter your email to get early access" />
                                    <div className="input-group-prepend">
                                        <button className="btn btn-primary futura-lt" type="button" onClick={sendMail}>Get early access</button>
                                    </div>
                                </div>
                            {/* <a href="https://forms.gle/BwGT2BodaXhefiav6" target="_blank" className="btn btn-primary futura-lt"> Join our beta test </a> */}
                            </li>
                        </ul>
                    </div> 
                
                </nav>
            </div>
        </Fragment>
    )
}