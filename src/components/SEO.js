import React from "react"
import { Helmet } from "react-helmet"
import PropTypes from "prop-types"
import { StaticQuery, graphql } from "gatsby"
// import SchemaOrg from "./SchemaOrg"
import favicon from "../assets/favicon/favicon.ico"


const SEO = ({ title, description, image, pathname, article }) => (
    <StaticQuery
      query={query}
      render={({
        site: {
          siteMetadata: {
            defaultTitle,
            titleTemplate,
            defaultDescription,
            siteUrl,
            defaultImage,
            twitterUsername,
          }
        }
      }) => {
        console.log(defaultTitle, defaultImage, image)
        const seo = {
          title: "Spincrow - Never wait on your clients for payment",
          description: description || defaultDescription,
          image: `${siteUrl}${image || defaultImage}`,
          url: `${siteUrl}${pathname || '/'}`,
          organization: title || defaultTitle
        }

        const canonical = siteUrl
        return (
            <>
            <Helmet 
              title={seo.title} 
              titleTemplate={titleTemplate}
              link={
                canonical
                ? [
                    {
                      rel: "canonical",
                      href: canonical,
                    },
                  ]
                : []
              }
              meta={[
                {
                  property: "og:image:width",
                  content: 1200,
                },
                {
                  property: "og:image:height",
                  content: 630,
                },
                {
                  name: 'description',
                  content: seo.description
                },
                {
                  name: 'image',
                  content: seo.image
                },
                {
                  property: 'og:type',
                  content: "website"
                },
                {
                  property: 'og:url',
                  content: seo.url
                },
                {
                  property: 'og:title',
                  content: seo.title
                },
                {
                  property: 'og:description',
                  content: seo.description
                },
                {
                  property: 'og:image',
                  content: seo.image
                },
                {
                  property: 'twitter:card',
                  content: "summary_large_image"
                },
                {
                  property: 'twitter:creator',
                  content: "@legoboxio"
                },
                {
                  property: 'twitter:site',
                  content: "https://legobox.io"
                },
                {
                  property: 'twitter:title',
                  content: seo.title
                },
                {
                  property: 'twitter:description',
                  content: seo.description
                },
                {
                  property: 'twitter:url',
                  content: seo.url
                },
                {
                  property: 'twitter:image',
                  content: seo.image
                },
                {
                  property: 'twitter:image:alt',
                  content: seo.title
                },
              ]}
            >
                <link rel="icon" href={favicon} />
                  {/* <!-- Global site tag (gtag.js) - Google Analytics --> */}
                  <script async src="https://www.googletagmanager.com/gtag/js?id=G-2Z9ZHV3PVY"></script>
                  <script>
                    window.dataLayer = window.dataLayer || [];
                    function gtag(){dataLayer.push(arguments);}
                    gtag('js', new Date());

                    gtag('config', 'G-2Z9ZHV3PVY');
                  </script>
            </Helmet>
            {/* <SchemaOrg 
              isBlogPost={false}
              url={seo.url}
              title={seo.title}
              image={seo.image}
              description={seo.description}
              datePublished={null}
              canonicalUrl={seo.url}
              author={null}
              organization={seo.organization}
              defaultTitle={seo.title}
            /> */}
        </>
        )
      }}
    />
  )

export default SEO

SEO.propTypes = {
  title: PropTypes.string,
  description: PropTypes.string,
  image: PropTypes.string,
  pathname: PropTypes.string,
  article: PropTypes.bool,
}
SEO.defaultProps = {
  title: null,
  description: null,
  image: null,
  pathname: null,
  article: false,
}

const query = graphql`
  query SEO {
    site {
      siteMetadata {
        defaultTitle: title
        titleTemplate
        defaultDescription: description
        siteUrl: url
        defaultImage: image
        instagramUsername
      }
    }
  }
`