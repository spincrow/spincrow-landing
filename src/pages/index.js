import React, {useEffect, useState} from "react"
import Layout from "@components/Layout"
import {graphql, Link} from "gatsby"
import Parallax from 'parallax-js'
import {sendSubscriberMail} from "../utils/helpers"


const Header = require('../assets/Header.svg')
const Header1a = require('../assets/Header-1-a.svg')
const Header1b = require('../assets/Header-1-b.svg')
const Header2 = require('../assets/Header2.svg')
const Header3 = require('../assets/Header3.svg')
const Header3a = require('../assets/Header3a.svg')
const Header3b = require('../assets/Header3b.svg')
const Header3c = require('../assets/Header3c.svg')
const Header4 = require('../assets/Header4.svg')

export default ({data}) => {

    const [state, updateState] = useState({
        email: null
    })

    const sendMail = async () => {
        // console.log("called mail")
        let resp = await sendSubscriberMail(state.email)
        // console.log(resp)
        updateState({
            ...state,
            email: null
        })
    } 

    const updateMail = (e) => {
        updateState({
            ...state,
            email: e.target.value
        })
    }

    return (
        <React.Fragment>
            <Layout>
                <section className="" >
                    <div style={{background: require('../assets/base-header.svg')}} className="container mt-4">
                        <div className="row justify-content-center mt-4 pt-5">
                            <div className="col-md-10 col-sm-12">
                                <h1 className="futura-lt display-md-5 display-sm-6 font-weight-bold text-center">
                                    Buying and selling online has never been <span className="emphasis text-underline">safer</span>
                                </h1>
                                <p className="font-weight-lighter text-secondary text-center">
                                    With Spincrow, you can carryout online transactions with ease and certainty that the safety of your funds and goods are guaranteed.
                                </p>
                                <div className="row justify-content-center">
                                    <div className="col-md-12 justify-content-center d-flex"  data-relative-input="true">
                                        <img className="d-none d-md-block" style={{width: '100%'}} src={Header} />
                                        <img className="d-block d-md-none mt-2 w-100"  src={Header} />
                                    </div>
                                </div>
                                <div className="row justify-content-center mt-4"> 
                                    <div className="col-md-8 col-12">
                                        <div className="input-group mb-3">
                                            <input type="text" value={state.email} onChange={updateMail} className="form-control" placeholder="Enter your email to get early access" />
                                            <div className="input-group-prepend">
                                                <button className="btn btn-primary futura-lt" type="button" onClick={sendMail}>Get early access</button>
                                            </div>
                                        </div>
                                    </div>
                                    {/* <a href="https://forms.gle/BwGT2BodaXhefiav6" target="_blank" className="btn btn-primary "> Join our beta test </a> */}
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                {/* <div className="container">
                    <div className="row mt-5 pt-5">
                        <div className="order-md-2 col-md-6 col-dm-12 mt-4 pt-5">
                            <h4 className="futura-lt display-6 font-weight-bold">
                                Assured payment for services
                            </h4>
                            <p className="font-weight-lighter small text-secondary" style={{fontSize: 20}}>
                                But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of.
                            </p>
                        </div>
                        <div className="order-md-1 col-md-6 col-dm-12">
                            <div className="row justify-content-center">
                            <div className="col-md-12 col-12">
                                    <div className="row">
                                        <div className="col-md-12 justify-content-center mt-sm-4 d-inline-flex" data-relative-input="true">
                                            <img data-depth="0.1" style={{width: '70%'}} src={Header2} />
                                        </div>
                                    </div>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="row vh-75" style={{marginTop: 80, background: 'rgba(36,180,126,0.15)'}}>
                    <div className="container py-5">
                        <div className="row my-3">
                            <div className="col-md-6 col-12 mt-4 pt-5">
                                <h4 className="futura-lt display-6 font-weight-bold">
                                    Decide when sellers are paid
                                </h4>
                                <p className="font-weight-lighter small text-secondary" style={{fontSize: 20}}>
                                    But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of.
                                </p>
                            </div>
                            <div className="col-md-6 col-12">
                                <div className="row justify-content-center">
                                    <div className="col-12">
                                            <div className="row">
                                                <div className="col-md-12 justify-content-center mt-sm-4 d-inline-flex">
                                                    <img style={{width: '70%'}} src={Header3} />
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="row vh-75" style={{marginTop: 80}}>
                    <div className="container py-5">
                        <div className="row">
                            <div className="col-md-6 col-dm-12">
                                <div className="row justify-content-center">
                                    <div className="col-12">
                                            <div className="row">
                                                <div className="col-md-12 justify-content-center mt-sm-4 d-inline-flex"  >
                                                    <img  style={{width: '70%'}} src={Header4} />
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-6 col-dm-12 mt-4 pt-5">
                                <h4 className="futura-lt display-6 font-weight-bold">
                                    Define the rules of payment
                                </h4>
                                <p className="font-weight-lighter small text-secondary" style={{fontSize: 20}}>
                                    But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of.
                                </p>
                            </div>
                        </div>
                    </div>
                </div> */}
            </Layout>
        </React.Fragment>
    )   
}