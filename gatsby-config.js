/**
 * Configure your Gatsby site with this file.
 *
 * See: https://www.gatsbyjs.org/docs/gatsby-config/
 */

module.exports = {
  /* Your site config here */
   /* Your site config here */
   siteMetadata: {
    title: `Spincrow`,
    titleTemplate: "%s ",
    description:"Never wait on your clients for payment anymore",
    url: "https://spincrow.com", // No trailing slash allowed!
    siteUrl: "https://spincrow.com",
    image: "/assets/spincrow-header.png", // Path to your image you placed in the 'static' folder
    twitterUsername: "@legoboxio",
    instagramUsername: "@legoboxio",
  },
  plugins : [
    'gatsby-plugin-sass',
    `gatsby-plugin-react-helmet`,
    {
      resolve: 'gatsby-plugin-robots-txt',
      options: {
        host: 'https://spincrow.com',
        sitemap: 'https://spincrow.com/sitemap.xml',
        policy: [{ userAgent: '*', allow: '/' }]
      }
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `src`,
        path: `${__dirname}/src/`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/static/assets/`,
      },
    },
    {
      resolve: 'gatsby-plugin-mailchimp',
      options: {
          endpoint: 'https://legobox.us15.list-manage.com/subscribe/post?u=fe4501185036bdfa2c3a2b22d&amp;id=f05f8bafea', // string; add your MC list endpoint here; see instructions below
          timeout: 3500, // number; the amount of time, in milliseconds, that you want to allow mailchimp to respond to your request before timing out. defaults to 3500
      }
    }
  ]
}
